<?php

namespace Dalnix\Report;

use Dalnix\Report\Commands\Report;
use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    protected $commands = [
        Report::class
    ];

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            /*
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('Report.php'),
            ], 'config');

            $this->loadViewsFrom(__DIR__.'/../resources/views', 'Report');

            $this->publishes([
                __DIR__.'/../resources/views' => base_path('resources/views/vendor/Report'),
            ], 'views');
            */
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->commands($this->commands);
        // $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'Report');
    }
}
