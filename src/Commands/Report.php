<?php

namespace Dalnix\Report\Commands;

use Illuminate\Console\Command;

class Report extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dalnix:report {from?} {to?} {--send : Send commits to endpoint} {--pipe : Read hash list from stdin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse git history and report time to Dalnix Customerpanel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        try {
            $this->env = file_get_contents(base_path('.env'));
        } catch (\Exception $e) {
            logger()->error('WARNING: .env file is missing');
        }

        if (preg_match_all("/^BILLING_ENDPOINT.*$/m", $this->env,
            $matches)) {
            $this->endpoint = rtrim(ltrim(explode('=', implode("\n", $matches[0]))[1], '""'), '"');
        } else {
            logger('WARNING: BILLING_ENDPOINT is missing your .env!');
        }

        if (preg_match_all("/^.*BILLING_TOKEN.*$/m", $this->env,
            $matches)) {
            $this->token = rtrim(ltrim(explode('=', implode("\n", $matches[0]))[1], '""'), '"');
        } else {
            logger()->error('WARNING: BILLING_TOKEN is missing your .env!');
        }
    }

    public $env;
    public $endpoint;
    public $token;

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $pipe = $this->option('pipe');

        if ($pipe) {// get hashes from stdin
            $stdin = stream_get_contents(STDIN);
            $hashes = array_filter(explode("\n", $stdin));
        } else {
            $after = $this->argument('from') ?: now()->startOfDay()->toDateTimeString();
            $before = $this->argument('to') ?: now()->endOfDay()->toDateTimeString();

            $pattern = "\[*\]";
            $git_log = shell_exec('git log --after="' . $after . '" --before="' . $before . '" --grep="' . $pattern . '" --pretty=%H');
            $hashes = array_filter(explode("\n", $git_log));
        }

        $count = count($hashes);

        $this->report($hashes);

        $this->line('');
        $this->line('');

        if ($pipe) {
            $this->line('I counted <fg=green>' . $count
                . '</> commits read from stdin');
        } else {
            $this->line('I counted <fg=green>' . $count . '</> commits between <fg=green>' . $after . '</> & <fg=green>' . $before . '</>');
        }

        if (!$this->option('send')) {
            $this->line('');
            $this->line('Use the <fg=green>--send</> option to send commits to ' . $this->endpoint);
        }
    }

    /**
     * Convert timetag, for example [1h20m], to minutes
     *
     * @param $time_tag
     *
     * @return int
     */
    function tagToMinutes($time_tag)
    {
        $minutes = $hoursInMinutes = $daysInMinutes = 0;

        // remove brackets
        $time_tag = str_replace('[', '', $time_tag);
        $time_tag = str_replace(']', '', $time_tag);

        // minutes
        if (preg_match('/[0-9]*m[a-z]*/', $time_tag, $m)) {
            $minutes = $m[0];
            $minutes = preg_replace('/\D/', '', $minutes);
        }

        // hours
        if (preg_match('/[0-9]*h[a-z]*/', $time_tag, $h)) {
            $hoursInMinutes = $h[0];
            $hoursInMinutes = preg_replace('/\D/', '', $hoursInMinutes);
            $hoursInMinutes = $hoursInMinutes * 60;
        }

        // days
        if (preg_match('/[0-9]*d[a-z]*/', $time_tag, $d)) {
            $daysInMinutes = $d[0];
            $daysInMinutes = preg_replace('/\D/', '', $daysInMinutes);
            $daysInMinutes = 8 * ($daysInMinutes * 60);
        }

        $total = $minutes + $hoursInMinutes + $daysInMinutes;

        return (int)$total;
    }

    /**
     * Send hashes to Billing Endpoint
     *
     * @param $hashes
     *
     * @throws \Exception
     */
    function report($hashes)
    {
        $this->line('Generating report data...');
        $this->line('');

        $total_minutes = 0;
        $hash_data = [];

        $bar = $this->output->createProgressBar(count($hashes));

        $bar->start();

        foreach ($hashes as $hash) {
            $time_tag = shell_exec('git log -1 ' . $hash . ' --pretty=%B 2> /dev/null | grep -i -e "\[*\]"');
            $time_tag = trim(preg_replace('/\s\s+/', ' ', $time_tag));

            // remove anything before and after brackets
            $time_tag = strstr($time_tag, '[');
            $time_tag = strstr($time_tag, ']', true);
            $time_tag .= ']';

            $minutes = $this->tagToMinutes($time_tag);
            $author = exec('git log -1 ' . $hash . ' --pretty="%an" 2>/dev/null', $author, $retval);
            $date = exec('git log -1 ' . $hash . ' --pretty="%ai" 2>/dev/null', $date, $retval);
            $timestamp = exec('git log -1 ' . $hash . ' --pretty="%at" 2>/dev/null', $timestamp, $retval);
            $email = exec('git log -1 ' . $hash . ' --pretty="%ae" 2>/dev/null', $email, $retval);
            $subject = base64_encode(exec('git log -1 ' . $hash . ' --pretty=%s 2>/dev/null', $subject, $retval));

            if ($retval !== 0) {
                $this->error('Something went wrong when running Git. Best guess; no commit exists with hash: '
                    . $hash . '. Aborting!');
                die();
            }

            $total_minutes += $minutes;

            // $this->line($date.' '.$minutes.' min '.$author);

            $hash_data[] = [
                'token' => $this->token,
                'hash' => $hash,
                'timestamp' => $timestamp,
                'minutes' => $minutes,
                'timetag' => $time_tag,
                'email' => $email,
                'subject' => $subject,
                'author' => $author,
                'date' => $date,
            ];

            $bar->advance();
        }

        $bar->finish();

        $this->line('');
        $this->line('');

        foreach (collect($hash_data)->groupBy('author') as $author => $commits) {
            $author_minutes = collect($commits)->sum('minutes');
            $author_hours = round($author_minutes / 60);
            $this->line($author . ': ~' . $author_hours . ' hour(s) (' . $author_minutes . ' minutes)');
        }

        if ($this->option('send')) {
            $this->send($hash_data);
        }
    }

    public function send($hashes)
    {
        $curl = curl_init();

        // Check if initialization had gone wrong*
        if ($curl === false) {
            throw new \Exception('failed to initialize');
        }

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->endpoint,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => [
                'token' => $this->token,
                'data' => json_encode($hashes),
            ],
        ]);

        $content = curl_exec($curl);

        // Check the return value of curl_exec(), too
        if ($content === false) {
            logger()->error(curl_error($curl));
            die(curl_errno($curl));
        } else {
            foreach (json_decode($content) as $line) {
                $this->line($line);
            }
        }
    }
}
