<?php

namespace Dalnix\Report;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Dalnix\Report\ReportClass
 */
class ReportFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Report';
    }
}
