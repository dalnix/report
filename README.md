# Dalnix Report tool for billing

Artisan command for reporting time via Git commits.

## Installation

You can install the package via composer:

```bash
composer require dalnix/report
```

## Usage

Add BILLING_ENDPOINT and BILLING_TOKEN to your `.env` and run:

```bash
php artisan dalnix:report
```

or if you want send commits from STDIN using a pipe. Just feed me with a list of hashes. Using `git log` you can filter 
all sorts of things; maybe you only track time on merge commits, then it's pretty pointless to parse all commits.

```bash
git log --format="%H" | php artisan dalnix:report --pipe
```

## FAQ

### I don't use Composer and/or my project isn't Laravel based

Use the legacy variant found in bin/report.php. Same rules apply about `BILLING_ENDPOINT` and `BILLING_TOKEN` in your
env if you want to send. Otherwise just run it.

```
php bin/report.php
```

### How should I format my commit messages?

Add a single "timecode" within your commit message. For instance

```text
Add a time consuming worm

Applying this commit will add a worm that lives on Time. Took one and a
half day to complete. Don't really know why I only made one commit. But
hey; living on the edge!

[2d4h33m]
```

It's up to you how to use it. Maybe you get billed for start of every hour; fine, only use the "hour" tag, ie `[4h]` and
skip the minutes. Or only use minutes, `[550m]` to show off your over the top math skills. 

### How to get a list of commits suitable for parsing?

```bash
git log --format="%H %ai %ae %f"
```

### How to list commits (hashes only) after a specific commit hash?

```bash
git rev-list <hash>^..HEAD
```

## Credits

- [Olle Gustafsson](https://www.dalnix.se/)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
