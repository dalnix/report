#!/usr/bin/env php
<?php

$env = @file('.env');

if ($env) {
    foreach ($env as $line) {
        if (substr($line, 0, strlen('BILLING_ENDPOINT')) === 'BILLING_ENDPOINT') {
            $endpoint = trim(explode('=', $line)[1]);
            echo "Found BILLING_ENDPOINT=$endpoint" . PHP_EOL;
        }

        if (substr($line, 0, strlen('BILLING_TOKEN')) === 'BILLING_TOKEN') {
            $token = trim(explode('=', $line)[1]);
            echo "Found BILLING_TOKEN=$token" . PHP_EOL;
        }
    }
}

if ( ! isset($argv[1])) {
    echo "Usage: " . $argv[0] . " <from> <to> [send]\n\n";
    echo "Any date format will do... for example:\n\n";
    echo "    " . $argv[0] . " 2016-01-01 2016-04-01\n\n";
    echo "    or\n\n";
    echo "    " . $argv[0] . " 1may 1jun\n\n";
    echo "to send to billing; add 'send' as a third parameter:\n\n";
    echo "    " . $argv[0] . " 2016-01-01 2016-04-01 send\n";
    exit(1);
}

$after   = $argv[1];
$before  = @$argv[2];
$send    = isset($argv[3]) ? true : false;
$pattern = "\[*\]";
$hashes  = array_filter(explode("\n", shell_exec('git log --after=' . $after . ' --before=' . $before . ' --grep="' . $pattern . '" --pretty=%H')));
$count   = count($hashes);

function report($hashes, $send)
{
    global $token;
    global $endpoint;

    $total_minutes = 0;

    foreach ($hashes as $hash) {
        $timetag   = shell_exec('git log -1 ' . $hash . ' --pretty=%B | grep -i -e "\[*\]"');
        $timetag   = trim(preg_replace('/\s\s+/', ' ', $timetag));
        // remove anything before and after brackets
        $timetag = strstr($timetag, '[');
        $timetag = strstr($timetag, ']', true);
        $timetag .= ']';

        $minutes   = tagToMinutes($timetag);
        $author    = exec('git log -1 ' . $hash . ' --pretty="%an"');
        $date      = exec('git log -1 ' . $hash . ' --pretty="%ai"');
        $timestamp = exec('git log -1 ' . $hash . ' --pretty="%at"');
        $email     = exec('git log -1 ' . $hash . ' --pretty="%ae"');
        $subject   = base64_encode(exec('git log -1 ' . $hash . ' --pretty=%s'));

        $total_minutes += $minutes;

        if ($send) {
            if (empty($token) || empty($endpoint)) {
                echo 'Check your .env. Missing BILLING_ENDPOINT or BILLING_TOKEN. Cannot send.' . PHP_EOL;
                die();
            }

            $curl  = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => $endpoint,
                CURLOPT_POST           => 1,
                CURLOPT_POSTFIELDS     => array(
                    'token'     => $token,
                    'hash'      => $hash,
                    'timestamp' => $timestamp,
                    'timetag'   => $timetag,
                    'email'     => $email,
                    'subject'   => $subject,
                )
            ));

            $resp = curl_exec($curl);
            curl_close($curl);
            echo $resp . PHP_EOL;

            if (strpos($resp, 'already saved') !== false) {
                $total_minutes -= $minutes;
            }
        } else {
            echo $date . ' ' . $minutes . ' min ' . $author . "\n";
        }
    }

    define('MINUTES_PER_HOUR', 60);

    $hours = intval($total_minutes / MINUTES_PER_HOUR);  // integer division
    $mins = $total_minutes % MINUTES_PER_HOUR;           // modulo

    printf("%d equals to [%02dh%02dm]\n", $total_minutes, $hours, $mins);

    echo $total_minutes . " minutes is about ";
    echo "~".round($total_minutes / 60) . " hours\n";
}

/**
 * Convert timetag, for example [1h20m], to minutes
 *
 * @param $timetag
 *
 * @return int
 */
function tagToMinutes($timetag)
{
    $minutes = $hoursInMinutes = $daysInMinutes = 0;

    // remove brackets
    $timetag = str_replace('[', '', $timetag);
    $timetag = str_replace(']', '', $timetag);

    // minutes
    if (preg_match('/[0-9]*m[a-z]*/', $timetag, $m)) {
        $minutes = $m[0];
        $minutes = preg_replace('/\D/', '', $minutes);
    }

    // hours
    if (preg_match('/[0-9]*h[a-z]*/', $timetag, $h)) {
        $hoursInMinutes = $h[0];
        $hoursInMinutes = preg_replace('/\D/', '', $hoursInMinutes);
        $hoursInMinutes = $hoursInMinutes * 60;
    }

    // days
    if (preg_match('/[0-9]*d[a-z]*/', $timetag, $d)) {
        $daysInMinutes = $d[0];
        $daysInMinutes = preg_replace('/\D/', '', $daysInMinutes);
        $daysInMinutes = 8 * ($daysInMinutes * 60);
    }

    $total = $minutes + $hoursInMinutes + $daysInMinutes;

    return (int)$total;
}

report($hashes, $send);

